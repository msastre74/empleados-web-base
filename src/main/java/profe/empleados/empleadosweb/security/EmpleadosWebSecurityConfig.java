package profe.empleados.empleadosweb.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

@EnableWebSecurity
@Configuration
public class EmpleadosWebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.eraseCredentials(false)
			.inMemoryAuthentication()
			.withUser("profe").password("{noop}profe").roles("USER")
				.and()
			.withUser("admin").password("{noop}admin").roles("ADMIN");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/gestEmpleados").hasAnyRole("USER", "ADMIN")
				.antMatchers("/listaDepartamentos").hasRole("ADMIN")
				.antMatchers("/").permitAll()
				.and()
			.formLogin()
				.and()
			.csrf().disable()
			.logout()
				.logoutSuccessUrl("/");			
	}
	
}
